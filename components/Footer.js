import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'
import React from 'react'
import Entypo from "react-native-vector-icons/Entypo"
import FontAwesome from "react-native-vector-icons/FontAwesome"
import Feather  from "react-native-vector-icons/Feather"



const Footer = ({navigation}) => {
  return (
    <View style={styles.container} >
        <TouchableOpacity style={styles.iconContainer} onPress={()=>navigation.navigate("home")}> 
          <Feather name="home" size={22} color="#eb2d4a" />
           <Text style={{color:"#eb2d4a"}}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconContainer} onPress={()=>navigation.navigate("categories")}> 
           <Feather name="grid" size={22} color="#b3b3b3" />
           <Text style={{color:"#b3b3b3"}}>Categories</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{...styles.iconContainer ,...styles.searchIcon}} onPress={()=>navigation.navigate("search")} > 
             <FontAwesome name="search" size={30} color="#eb2d4a" />
             {/* <Text>search</Text> */}
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconContainer} onPress={()=>navigation.navigate("cart")}> 
            <Feather name="shopping-cart" size={22} color="#b3b3b3" />
            <Text style={{color:"#b3b3b3"}}>Cart</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.iconContainer} onPress={()=>navigation.navigate("profile")}> 
            <Feather name="user" size={22} color="#b3b3b3" />
            <Text style={{color:"#b3b3b3"}}>Profile</Text>
        </TouchableOpacity>
    </View>
  )
}

export default Footer

const styles =StyleSheet.create({
  container:{
    width:"106%",
    height:65,
    flexDirection:"row",
    textAlign:"center",
    justifyContent:"space-between",
    paddingTop:12,
    // shadowColor:"#7f5df0",
    borderRadius:10,
    backgroundColor:"#fff",
    marginBottom:-20,
    paddingBottom:10,
    marginLeft:-12,
    alignItems:"center",
    // padding:0,
    shadowOffset:{
      width:0,
      height:1
    },
    shadowOpacity:0.25,
    shadowRadius:2,
    elevation:5
  },
  iconContainer:{
    width:"20%",
    alignItems:"center"
  },searchIcon:{
    //  borderColor:"red" , 
    //  borderWidth:1 , 
    //  borderStyle:"solid",
     width:70,
     height:70,
     borderRadius:35,
     justifyContent:"center",
     alignItems:"center",
     top:-30,
     backgroundColor:"#fff",
     shadowColor:"#7f5df0",
     shadowOffset:{
      width:0,
      height:10
     },
     shadowOpacity:0.25,
     shadowRadius:3.5,
     elevation:5,
    
  }
})