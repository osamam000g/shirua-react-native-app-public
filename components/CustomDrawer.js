import { View, Text, StyleSheet, ScrollView } from 'react-native'
import React from 'react'
import { DrawerContentScrollView, DrawerItemList } from '@react-navigation/drawer'
import Feather from "react-native-vector-icons/Feather"
import Ionicons from "react-native-vector-icons/Ionicons"

// import { ScrollView } from 'react-native-gesture-handler'
const CustomDrawer = (props) => {
  return (
    <View style={styles.container}>
        <View style={styles.userHolder}>
            <View style={styles.iconHolder}>
                <Feather name="user" size={35} color="#fff" />
            </View>
            <View style={styles.nameHolder} >
                <Text style={{fontSize:18}}>Mohamed Osama</Text>
            </View>
        </View>
        
        <ScrollView style={styles.bodyHolder}>
             <View {...props}>
                <DrawerItemList {...props} />
            </View>
         
        </ScrollView>
        <View style={styles.footer}>
            <Ionicons name="exit-outline" size={25} />
                <Text style={{marginLeft:10 , fontSize:18}}>
                    LogOut
                </Text>
            </View>
    </View>
  )
}

export default CustomDrawer

const styles = StyleSheet.create({
    container:{
        padding:15,
        marginTop:40,
        // alignItems:"center",
        flex:1
    },iconHolder:{
        backgroundColor:"#b3b3b3",
        width:70,
        height:70,
        borderRadius:35,
        alignItems:"center",
        justifyContent:"center"
    },userHolder:{
        flexDirection:"row",
        justifyContent:"space-around"
    },nameHolder:{
        justifyContent:"center",
        fontSize:18,
    },bodyHolder:{
//         justifyContent:"center",
flex:1,
marginTop:50,
// height:"100%"
    }
    ,footer:{
        padding:20,
        borderTopWidth:1,
        borderTopColor:"#ccc",
        // borderBottomWidth:1,
        // borderBottomColor:"#ccc",
        flexDirection:"row",
        justifyContent:"flex-start",
        alignItems:"center",
        marginBottom:-15,
    }
})