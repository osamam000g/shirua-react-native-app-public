import { View, Text , StyleSheet, Pressable} from 'react-native'
import React from 'react'
import Entypo from "react-native-vector-icons/Entypo"
import FontAwesome from "react-native-vector-icons/FontAwesome"


const ChatHeader = ({setModalVisible}) => {
  return (
    <View style={styles.container}>
            <Pressable>
                <Text onPress={()=>setModalVisible(false)} style={styles.buttomText}>
                   <FontAwesome name="close" size={25} color="#fff" /> Close
                </Text>
            </Pressable>
            <Text style={styles.heading}>
                    Chat
            </Text>
            <Entypo name="bell" size={25} color="#efefef" />
    </View>
  )
}

export default ChatHeader

const styles = StyleSheet.create({
    container:{
        flexDirection:"row",
        justifyContent:"space-between",
        // backgroundColor:"#000",
        paddingVertical:20,
        paddingHorizontal:10
        
    },
    heading:{
        color:"#fff",
        fontSize:20,
        fontWeight:"700"
    },
    buttomText:{
        color:"#fff",
        fontSize:20
    }
})