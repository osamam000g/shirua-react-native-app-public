import React from 'react'
import { Text, View  , StyleSheet , TouchableOpacity, Image} from 'react-native'
import FontAwesome from "react-native-vector-icons/FontAwesome"

const items = [
  {
    id:1,
    name:require("../assets/shirt.png"),
    title:"Shirt",
    // color:"#ff751f"
  }
  ,
  {
    id:2,
    name:require("../assets/electronics.png"),
    title:"Electronics",
  },
  {
    id:3,
    name:require("../assets/smartphone.png"),
    title:"Phones",
  },
  {
    id:4,
    name:require("../assets/trousers.png"),
    title:"pants",
  },

]

function MenuButtons({navigation}) {
  const openMeeting = ()=>{
    navigation.navigate('Room')
  }
  return (
    <View>

    <View style={styles.head1}>
      <Text style={styles.headerText}>
        Categories
      </Text>
      <Text style={styles.seeAllText}>
        See All
      </Text>
  </View>
    <View style={styles.container}>
    
      {
        items.map((item , index)=>
          <View  key={index} style={styles.buttonContainer}>

          <TouchableOpacity onPress={()=>openMeeting()} style={{...styles.button , backgroundColor:item.color ? item.color: "#fff"}}>
            {/* <FontAwesome name={item.name} size={23} color="#efefef" /> */}
            <Image source={item.name} />
          </TouchableOpacity>
          <Text style={styles.menuText}>{item.title}</Text>
  
        </View>
        )
      }
    </View>
     
    </View>
  )
}

export default MenuButtons

const styles = StyleSheet.create({
  container:{
    marginTop:25,
    // paddingBottom:10,
    // borderBottomColor:"#1f1f1f",
    // borderBottomWidth:1 ,
    flexDirection:"row",
    justifyContent:"space-between",
    
  },
  buttonContainer:{
    alignItems:"center",
    flex:1
  },
  button:{
    width:60,
    height:60,
    borderRadius:100,
    padding:5,
    justifyContent:"center",
    alignItems:"center"
  },
  menuText:{
      color:'#858585',
      fontSize:13,
      paddingTop:10,
      fontWeight:"600"
  }, head1:{
    flexDirection:"row",
    justifyContent:"space-between",
    marginTop:10,
    marginHorizontal:10
  },headerText:{
    fontSize:22,

  },
  seeAllText:{
    color:"#A09f9f",
    marginTop:7
  }
})