import React from 'react'
import { Text, View , StyleSheet, Image} from 'react-native'
import AntDesign from "react-native-vector-icons/AntDesign"


const contactsMenuButtons  =
[

    {
        type:"starred",
    },
    {
        type:"contact",
        name:"Jessy Khatab",
        // photo:require("../assets/g1.jpg")
    },
    {
        type:"contact",
        name:"nansy farid",
        // photo:require("../assets/g2.jpg")
    }
]

function ContactsMenu() {
  return (
    <View style={styles.container}>
{
    contactsMenuButtons.map((item,index)=>
    <View key={index} style={styles.row}>
            <View style={styles.starredIcon}> 
              { item.type == "starred" ?  <AntDesign name="star" size={20} color="#efefef" /> : <Image source={item.photo} style={styles.image} />  }
            </View>
        <Text style={styles.text}>{item.type == "starred" ? "Starred" : item.name}</Text>
    </View>
    
    )
}
      

    </View>
  )
}

export default ContactsMenu

const styles = StyleSheet.create({
    row:{
        flexDirection:"row",
        marginTop:20,
        alignItems:"center"
    },
    starredIcon:{
        backgroundColor:"#333",
        width:55,
        height:55,
        justifyContent:"center",
        alignItems:"center",
        borderRadius:20
    },
    text:{
        color:"#fff",
        paddingLeft:15,
        fontSize:18
    },
    image:{
        width:55,
        height:55,
        borderRadius:20
    }

})