import { View, Text, Image, StyleSheet, ScrollView, TouchableOpacity } from 'react-native'
import React from 'react'

const CategoryList = () => {
  return (
    <View style={{marginTop:20}}>     
    <ScrollView  style={styles.container} showsHorizontalScrollIndicator={false}>
      {/* <Text>HomeSlider</Text> */}
      <TouchableOpacity style={styles.imageHolder}>
         <Image style={styles.image} source={require("../assets/s1.png")} />
         <Text style={styles.caText}>Shoes</Text>
      </TouchableOpacity>
      <TouchableOpacity>
         <Image style={styles.image} source={require("../assets/s1.png")} />
         <Text style={styles.caText}>cloth</Text>

      </TouchableOpacity>
    </ScrollView>
    </View>
  )
}

export default CategoryList
const styles = StyleSheet.create({
  container:{
      flex:1,
      
  },
  image:{
      width:"99%",
      height:200,
      borderRadius:5,
      // marginLeft:7,
      marginTop:10,
      shadowColor:"#7f5df0",
      shadowOffset:{
       width:0,
       height:10
      },
      shadowOpacity:0.25,
      shadowRadius:3.5,
      elevation:5,
  },imageHolder:{
    alignItems:"center",   
  },caText:{
    alignSelf:"flex-start",
    marginLeft:5,
    fontSize:18,
    textTransform:"uppercase",
    color:"#000",
    shadowColor:"#000",
    shadowOffset:{
      width:0,
      height:10
    },
    shadowOpacity:0.25,
    elevation:1,
    bottom:50,
    fontWeight:"bold",
    letterSpacing:10,
    borderWidth:1,
    borderColor:"#fff",
    borderRadius:5,
    padding:10,
    textAlign:"center",
    backgroundColor:"#fff",
    marginBottom:-40
  }
})