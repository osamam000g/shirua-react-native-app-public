import React, { useCallback, useState } from 'react'
import { Text, View  , StyleSheet , TouchableOpacity, Image, ScrollView} from 'react-native'
import Feather from "react-native-vector-icons/Feather"
import AntDesign from "react-native-vector-icons/AntDesign"
import Animated, { interpolate, useAnimatedGestureHandler, useAnimatedStyle, useSharedValue, withSpring } from 'react-native-reanimated'
import { PanGestureHandler, PanGestureHandlerGestureEvent } from 'react-native-gesture-handler'
import { runOnJS } from 'react-native-reanimated/lib/reanimated2/core'
import CartSingleItem from './CartSingleItem'

const items = [
  {
    id:1,
    name:require("../assets/s1.png"),
    title:"Shirt",
    price:"100"
  }
  ,
  {
    id:2,
    name:require("../assets/s1.png"),
    title:"Shirt",
    price:"100"
  }
 

]
const buttonHieght = 100 ; 

const clamp = (value: number , min:number , max:number)=>{
  'worklet';
  return Math.min(Math.max(value , min ) , max )
} 

const CartItems = () => {
  const MAX_SLIDE_OFFSET = buttonHieght * .3 ;
 


  const translationyShare = useSharedValue(0)
  const translationXShare = useSharedValue(0)
  const [quantityCount , setquantityCount] = useState(0)



  const incrementQuantity = useCallback(()=>{
    setquantityCount((currentCount)=>currentCount   - 1)

  },[])

  const decrementCount = useCallback(()=>{
    setquantityCount((currentCount)=>currentCount   + 1)

  },[])

  const resetCount = useCallback(()=>{
    setquantityCount((currentCount)=> 0 )

  },[])

  const onPanEvent = useAnimatedGestureHandler<PanGestureHandlerGestureEvent>({
    onActive: (event) =>{
      translationyShare.value = clamp(event.translationY , -MAX_SLIDE_OFFSET , MAX_SLIDE_OFFSET)
      translationXShare.value = clamp(event.translationX , -MAX_SLIDE_OFFSET , 0 )
      // translationXShare.value = event.translationY
    },

    onEnd: ()=>{
      translationyShare.value = withSpring(0)
      translationXShare.value = withSpring(0)

      if(translationyShare.value === MAX_SLIDE_OFFSET){

        if (quantityCount <= 0){
          runOnJS(resetCount)();
        }else{
        runOnJS(incrementQuantity)();

        }
      }else if (translationyShare.value === -MAX_SLIDE_OFFSET) {
        // Decrement
        runOnJS(decrementCount)();
      }else if (translationXShare.value === -MAX_SLIDE_OFFSET) {
        runOnJS(resetCount)();
      }
    }
  })
  const rStyle = useAnimatedStyle(()=>{
    return {
      transform:[{translateY: translationyShare.value }  , {translateX:translationXShare.value}]
    }
  },[])

  const rPlusMinusIconStyle = useAnimatedStyle(()=>{
    const opacityY = interpolate(translationyShare.value ,  [-MAX_SLIDE_OFFSET , 0 , MAX_SLIDE_OFFSET] , [0.4 , 1 , 0.4])

    const opacityX = interpolate(translationXShare.value , 
      [-MAX_SLIDE_OFFSET , 0 ] 
      , [0 , 1])


    return {
      opacity:opacityY * opacityX
    }
  })

  const rCloseIconStyle= useAnimatedStyle(()=>{
    const opacity = interpolate(translationXShare.value , 
       [-MAX_SLIDE_OFFSET , 0 ] 
       , [0.8 , 0])
    return {
      opacity
    }
  })


  const IconSize = 20 ;
  return (
    <View style={{marginTop:25}}>

          <View style={styles.head1}>
            <Text style={styles.headerText}>
              Total Price 
            </Text>
            <Text style={styles.seeAllText}>
              200 L.E
            </Text>
        </View>
        <View >
            <ScrollView showsHorizontalScrollIndicator={false}>
              <View  style={styles.container}>
                      
                      {
                      items.map((item , index)=>
                       <View  key={index} style={styles.buttonContainer}>
                          

                          <TouchableOpacity onPress={()=>console.log('hi')} style={{...styles.button }}>
                              <Image source={item.name}  style={styles.button }/>
                          </TouchableOpacity>
                          <TouchableOpacity>
                              <Text style={styles.menuText}>{item.title}</Text>
                            <View style={styles.priceHolder}>
                                  <Text style={styles.menuText2}>90 L.E </Text>
                                  <Text style={styles.menuText1}>{item.price} L.E </Text>
                            </View>
                            {/* <View style={styles.plusMinusHolder}>
                              <Feather name="plus" size={17} color="#000" />
                                <Text style={styles.textPlus}> 1 </Text>
                              <Feather name="minus" size={17} color="#000" />
                          </View> */}

                          </TouchableOpacity>

                          {/* <View style={styles.newPlusMinusHolder}>
                            
                            <Animated.View style={rPlusMinusIconStyle}>
                              <AntDesign name="plus" color="white"  size={IconSize}/>
                            </Animated.View>
                            <Animated.View style={rCloseIconStyle}>
                              <AntDesign name="close" color="white" size={IconSize}  />
                          </Animated.View>
                            <Animated.View style={rPlusMinusIconStyle}>
                               <AntDesign name="minus" color="white" size={IconSize} />
                            </Animated.View>


                           <View style={{...StyleSheet.absoluteFillObject , justifyContent:"center" ,alignItems:"center"}}>
                            <PanGestureHandler onGestureEvent={onPanEvent}>
                                <Animated.View style={[styles.counterCircle , rStyle]} >
                            <Text style={styles.QuantityCount}>{quantityCount}</Text>

                                  </Animated.View>  
                            </PanGestureHandler>

                          </View> 
                          </View>
                          
                           */}

                           <CartSingleItem />

                      </View>
                      )
                      }
                  </View>
            </ScrollView>
        
        </View>
        
    </View>
  )
}

export default CartItems

const styles = StyleSheet.create({
  QuantityCount:{
    color:"#fff",

  }
  ,
counterCircle:{
  width:30,
  height:30 ,
  borderRadius:100,
  backgroundColor:"#232323",
  position:"absolute", 
  justifyContent:"center",
  alignItems:"center"

}
  ,
  newPlusMinusHolder:{
      width:50,
      height:buttonHieght,
      backgroundColor:"#eb2d4a",
      borderRadius:100,
      justifyContent:"space-evenly",
      alignContent:"center",
      alignItems:"center",
      // paddingVertical:10

  },
    container:{
      marginTop:15,
      // flex:1
    },
    buttonContainer:{
        marginLeft:7,
        flexDirection:"row",
        // marginBottom:20,
        borderColor:"#ccc",
        borderBottomWidth:1,
        paddingBottom:10,
        paddingTop:10,
        paddingLeft:0,
        justifyContent: "space-between",
        paddingRight:0,
      backgroundColor:"#fff",
    marginHorizontal:20


    },
    button:{
      width:100,
      height:100,
      borderRadius:5,
      padding:5,
      justifyContent:"center",
      alignItems:"center",
      marginRight:10,
      alignSelf:"center"
    },
    menuText:{
        color:'#858585',
        fontSize:20,
        paddingTop:10,
        fontWeight:"600",
        // alignSelf:"center"
        marginLeft:20
    }, 
    head1:{
      flexDirection:"row",
      justifyContent:"space-between",
      marginTop:10,
      // marginHorizontal:10,
      backgroundColor:"#fff",
      borderRadius:5,
      paddingHorizontal:20,
      height:40,
      alignItems:"center"
      
      
    },headerText:{
      fontSize:22,
  
    },
    seeAllText:{
      color:"#rgb(66, 186, 150)",
      fontSize:20,
      fontWeight:"bold",
      // marginTop:7,
      marginLeft:20
    },priceHolder:{
        flexDirection:"row",
        // justifyContent:"space-evenly"
    },
    menuText2:{
        fontSize:12,
        textDecorationLine:"line-through",
        color:"#bbb",
        marginRight:5,
        marginLeft:20,
        marginTop:10

    },
    menuText1:{
        fontSize:16,
        marginTop:10,

        fontWeight:"bold"
    },
    plusMinusHolder:{
      // borderColor:"gray",
      // borderWidth:1,
      // borderStyle:"solid",
      justifyContent:"space-around",
      alignContent:"center", 
      borderRadius:7,
      width:30,
      alignItems:"center",
      backgroundColor:"#fff",
      shadowColor:"#000",
      shadowOffset:{
        width:0,height:10
      },
      shadowRadius:5,
      shadowOpacity:0.25,
      elevation:5
    }
  })