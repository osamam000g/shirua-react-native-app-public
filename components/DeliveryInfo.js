import { View, Text, StyleSheet, Dimensions, TextInput, TouchableOpacity } from 'react-native'
import React from 'react'
import FontAwesome from "react-native-vector-icons/FontAwesome"



const width = Dimensions.get("screen").width

const DeliveryInfo = () => {
  return (
    <View style={styles.container}>
        <View >
             <Text style={styles.Title}>Your delivery info:</Text>
        </View>
        <View>
            <View style={{marginTop:0 , marginHorizontal:20}}>
                <View style={styles.inputHolder}> 
                    <FontAwesome name="user" size={20} style={styles.inputIcon}/>
                    <TextInput  placeholder='Your Name' />
                </View>
                <View style={styles.inputHolder}> 
                    <FontAwesome name="globe" size={20} style={styles.inputIcon}/>
                    <TextInput  placeholder='Country' />
                </View>
                <View style={styles.inputHolder}>
                    <FontAwesome name="map-pin" size={20} style={styles.inputIcon}/>
                    <TextInput placeholder='City' secureTextEntry={true}/>
                </View>
                <View style={styles.inputHolder}>
                    <FontAwesome name="map" size={20} style={styles.inputIcon}/>
                    <TextInput placeholder='Address' secureTextEntry={true}/>
                </View>
                <View style={styles.inputHolder}>
                    <FontAwesome name="envelope" size={20} style={styles.inputIcon}/>
                    <TextInput placeholder='Email' secureTextEntry={true}/>
                </View>
                <View style={styles.inputHolder}>
                    <FontAwesome name="phone" size={20} style={styles.inputIcon}/>
                    <TextInput placeholder='phone number' secureTextEntry={true}/>
                </View>
                
            </View>
        </View>
    </View>
  )
}

export default DeliveryInfo

const styles = StyleSheet.create({
    container:{
        width:width,
        padding:15,
        flex:1
    },
    Title:{
        fontSize:18,
        fontWeight:"bold",
        marginTop:18
    },
    inputHolder:{
        borderBottomColor:"gray",
        borderBottomWidth:1,
        flexDirection:"row",
        height:40,
        alignItems:"center",
        padding:5,
        borderRadius:5,
        marginTop:30,
        width:width-100
    },inputIcon:{
        marginRight:10
    },loginBtn:{
        marginTop:30 ,
        // marginHorizontal:20,
        width:width-70,
        backgroundColor:"#eb2d4a",
        color:"#fff",
        height:40,
        textAlignVertical:"center",
        textAlign:"center",
        borderRadius:5,
        fontSize:18,
        textTransform:"uppercase"  
    },
    linebreakHolder:{
            marginTop:20,
            justifyContent:"space-between",
            flexDirection:"row",
            height:20,
            alignItems:"center"
    },

})