import React from 'react'
import { Text, View  , StyleSheet , TouchableOpacity, Image, ScrollView} from 'react-native'
import FontAwesome from "react-native-vector-icons/FontAwesome"

const items = [
  {
    id:1,
    name:require("../assets/s1.png"),
    title:"Shirt",
    price:"100"
  }
  ,
  {
    id:2,
    name:require("../assets/s1.png"),
    title:"Shirt",
    price:"100"
  }
  ,
  {
    id:3,
    name:require("../assets/s1.png"),
    title:"Shirt",
    price:"100"
  },{
    id:4,
    name:require("../assets/s1.png"),
    title:"Shirt",
    price:"100"
  },{
    id:5,
    name:require("../assets/s1.png"),
    title:"Shirt",
    price:"100"
  }
//   {
//     id:2,
//     name:require("../assets/electronics.png"),
//     title:"Electronics",
//   },
//   {
//     id:3,
//     name:require("../assets/smartphone.png"),
//     title:"Phones",
//   },
//   {
//     id:4,
//     name:require("../assets/trousers.png"),
//     title:"pants",
//   },

]

const NewArrival = ({navigation}) => {
  const openMeeting = () =>{
    navigation.navigate("single")

  }

  return (
    <View style={{marginTop:25}}>

    <View style={styles.head1}>
      <Text style={styles.headerText}>
        New Arrival
      </Text>
      <Text style={styles.seeAllText}>
        See All
      </Text>
  </View>
  <View >
       <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        <View  style={styles.container}>
                
                {
                items.map((item , index)=>
                    <View  key={index} style={styles.buttonContainer}>

                    <TouchableOpacity onPress={()=>openMeeting()} style={{...styles.button , backgroundColor:item.color ? item.color: "#fff"}}>
                    {/* <FontAwesome name={item.name} size={23} color="#efefef" /> */}
                    <Image source={item.name}  style={styles.button }/>
                    </TouchableOpacity>
                    <Text style={styles.menuText}>{item.title}</Text>
                   <View style={styles.priceHolder}>
                        <Text style={styles.menuText2}>90 L.E </Text>
                        <Text style={styles.menuText1}>{item.price} L.E </Text>
                   </View>

                </View>
                )
                }
            </View>
       </ScrollView>
   
  </View>
    </View>
  )
}

export default NewArrival

const styles = StyleSheet.create({
    container:{
      marginTop:25,
      // paddingBottom:10,
      // borderBottomColor:"#1f1f1f",
      // borderBottomWidth:1 ,
      flexDirection:"row",
      justifyContent:"space-between",
      flex:1
      
    },
    buttonContainer:{
    //   alignItems:"center",
    //   flex:1,
    // width:"100%",
    marginLeft:7
    },
    button:{
      width:150,
      height:150,
      borderRadius:5,
      padding:5,
      justifyContent:"center",
      alignItems:"center"
    },
    menuText:{
        color:'#858585',
        fontSize:20,
        paddingTop:10,
        fontWeight:"600",
        alignSelf:"center"
    }, 
    head1:{
      flexDirection:"row",
      justifyContent:"space-between",
      marginTop:10,
      marginHorizontal:10
    },headerText:{
      fontSize:22,
  
    },
    seeAllText:{
      color:"#A09f9f",
      marginTop:7
    },priceHolder:{
        flexDirection:"row",
        // justifyContent:"space-evenly"
    },
    menuText2:{
        fontSize:12,
        textDecorationLine:"line-through",
        color:"#bbb",
        marginRight:5
    },
    menuText1:{
        fontSize:16,

        fontWeight:"bold"
    }
  })