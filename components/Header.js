import React from 'react'
import { Text, View , StyleSheet, TouchableOpacity, Image} from 'react-native'
import Entypo from "react-native-vector-icons/Entypo"
import Feather  from "react-native-vector-icons/Feather"
function Header({navigation , title }) {
  return (
        <View style={styles.container}>
            <TouchableOpacity style={styles.list} onPress={()=>navigation.openDrawer()}>
                 <Entypo name='list' size={30} color='#b3b3b3'/>
            </TouchableOpacity>
            {  title == "home" ?  <Image source={require("../assets/logo.png")} style={styles.logo} /> :<Image source={require("../assets/logo.png")} style={styles.logo} />}
            <View style={styles.rightContainer}>
                <TouchableOpacity style={styles.iconContainer}>
                   <Feather name='shopping-cart' size={22} color='#b3b3b3'/>
                </TouchableOpacity>
                <TouchableOpacity  style={styles.iconContainer}>
                    <Feather name="heart"  size={22} color="#b3b3b3"/>
                </TouchableOpacity>
            </View>
           
           
        </View>
  )
}

export default Header

const styles =StyleSheet.create({
    container:{
        // flex:1,

        flexDirection:"row",
        justifyContent:"space-between",
        // alignItems:"center",
        paddingVertical:20,
        paddingHorizontal:10
    },
    header:{

        color:'#000',
        fontSize:18,
        fontWeight:"700",
        width:"33.3%"
    },
    rightContainer:{
        flexDirection:"row",
        justifyContent:"flex-end",
        width:"33.3%",
        // paddingHorizontal:20
    
        // alignItems:"center",
    },
    iconContainer:{
        // justifyContent:"space-evenly",
       paddingHorizontal:7
    },
    list:{
        width:"33.3%"

    },
    logo:{
        width:"33.3%",
        height:40
    }

})