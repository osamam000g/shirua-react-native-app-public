import { View, Text, Image, StyleSheet, ScrollView, TouchableOpacity } from 'react-native'
import React from 'react'

const HomeSlider = () => {
  return (
    <View style={{height:200 , marginTop:20}}>     
    <ScrollView  horizontal={true} style={styles.container} showsHorizontalScrollIndicator={false}>
      {/* <Text>HomeSlider</Text> */}
      <TouchableOpacity>
         <Image style={styles.image} source={require("../assets/s1.png")} />
      </TouchableOpacity>
      <TouchableOpacity>
         <Image style={styles.image} source={require("../assets/s1.png")} />
      </TouchableOpacity>
    </ScrollView>
    </View>

  )
}

export default HomeSlider

const styles = StyleSheet.create({
    container:{
        // flexDirection:"row",
        // marginBottom:10,
        // marginTop:15,
        // height:200
        
    },
    image:{
        width:250,
        height:200,
        borderRadius:5,
        marginLeft:7,
        
    }
})