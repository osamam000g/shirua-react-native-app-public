import React from 'react'
import { Text, View , StyleSheet, TouchableOpacity} from 'react-native'
import Entypo from "react-native-vector-icons/Entypo"
import Feather  from "react-native-vector-icons/Feather"
function HeaderTransparent({navigation , title }) {
  return (
        <View style={styles.container}>

            <TouchableOpacity style={{...styles.list , ...styles.backgroundTrans}} onPress={()=>navigation.goBack()}>
                 <Feather name='arrow-left' size={30} color='#b3b3b3'/>
            </TouchableOpacity>
            {/* {  title == "home" ?   <Text style={styles.header}>LOGO Shirua </Text> : <Text style={styles.header}> {title} </Text>} */}
            <View style={{...styles.rightContainer }}>
                <TouchableOpacity style={{...styles.iconContainer , ...styles.backgroundTrans }}>
                   <Feather name='shopping-cart' size={22} color='#fff' />
                </TouchableOpacity>
                <TouchableOpacity  style={{...styles.iconContainer , ...styles.backgroundTrans }}>
                    <Feather name="heart"  size={22} color="#b3b3b3"/>
                </TouchableOpacity>
            </View>
           
           
        </View>
  )
}

export default HeaderTransparent

const styles =StyleSheet.create({
    container:{
        // flex:1,
        backgroundColor:"transparent",
        flexDirection:"row",
        justifyContent:"space-between",
        // alignItems:"center",
        paddingVertical:20,
        paddingHorizontal:10,
        zIndex:2,
        top:20
        // paddingTop:25
    },
    header:{

        color:'#000',
        fontSize:18,
        fontWeight:"700",
        width:"33.3%"
    },
    rightContainer:{
        flexDirection:"row",
        justifyContent:"flex-end",
        width:"44.3%",
        // paddingHorizontal:20
    
        // alignItems:"center",
    },
    iconContainer:{
        // justifyContent:"space-evenly",
       paddingHorizontal:7
       ,
       marginLeft:10
    },
    list:{
        width:"44.3%"

    },
    backgroundTrans:{
        backgroundColor:"#000",
        // opacity:0.50,
        width:50,
        height:50,
        borderRadius:30,
        justifyContent:"center",
        alignItems:"center"
    }

})