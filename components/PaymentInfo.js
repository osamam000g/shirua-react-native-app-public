import { View, Text, Image, StyleSheet, Dimensions, Touchable, Pressable  } from 'react-native'
import React, { useState } from 'react'
import Checkbox from 'expo-checkbox';



const width = Dimensions.get("screen").width
const PaymentInfo = () => {
    const [whoIsChecked , setWhoIsChecked ]=  useState("cash")


  return (
    <View style={styles.container}>
        <View >
          <Text style={{fontSize:18,fontWeight:"bold" , marginBottom:20}}>Select Payment Method:</Text>
        </View>
        <View style={styles.bodyContainer}>
            <Pressable style={styles.CheckboxHolder} onPress={()=>setWhoIsChecked("cash")}>
                  <Checkbox value={ whoIsChecked =="cash" ? true : false} style={styles.checkbox}  />
                <Text style={{fontSize:20,fontWeight:"bold" , marginVertical:20 , textAlignVertical:"center"}}>  
                     Cash On Delivery
                </Text>
            </Pressable>
            <Pressable style={styles.CheckboxHolder} onPress={()=>setWhoIsChecked("fawry")}>
               <Checkbox value={whoIsChecked == "fawry" ? true : false}  style={styles.checkbox} />   
                <Image style={styles.image} source={require("../assets/fawry.png")}  />
            </Pressable>
            <Pressable style={styles.CheckboxHolder} onPress={()=>setWhoIsChecked("visa")}>
               <Checkbox value={whoIsChecked == "visa" ? true : false}  style={styles.checkbox} />   
                <Image style={styles.image} source={require("../assets/visa.png")}  />
            </Pressable>

            { whoIsChecked =="cash" ? <View>
                <Text style={styles.titlePay}> Pay with cash upon delivery. </Text>
                <View>
                    <View style={styles.tableRowHolder}>
                        <Text style={styles.tableTitle}>
                            Subtotal
                        </Text>
                        <Text style={styles.tableNumber}>
                            416 L.E
                        </Text>
                    </View>
                    <View style={styles.tableRowHolder}>
                        <Text style={styles.tableTitle}>
                            Shipping
                        </Text>
                        <Text style={styles.tableNumber}>
                            20 L.E
                        </Text>
                    </View>
                   <View style={styles.linebreak}></View>

                    <View style={styles.tableRowHolder}>
                        <Text style={styles.tableTitle}>
                            Total
                        </Text>
                        <Text style={styles.tableNumber}>
                            435 L.E
                        </Text>
                    </View>
                </View>
            </View> : whoIsChecked =="fawry" ?
            <View>
                    <Text>
                        Fawry View
                    </Text>
            </View> : 
            <View>
                    <Text>
                        Visa View
                    </Text>
            </View>}
        </View>
    </View>
  )
}

export default PaymentInfo

const styles = StyleSheet.create({
    container:{
            width:width-30,
            padding:15,
            // backgroundColor:"red"
    },
    image:{
        width:170,
        height:45
    }, bodyContainer:{
    justifyContent:"flex-start"
    },CheckboxHolder:{
        flexDirection:"row",
        justifyContent:"flex-start",
        alignItems:"center",
        marginTop:10,
        textAlignVertical:"center"
    },checkbox:{
        marginRight:10
    }, linebreakHolder:{
        marginTop:20,
        justifyContent:"space-between",
        flexDirection:"row",
        height:20,
        alignItems:"center"
},
linebreak:{
    width:width-60,
    borderBottomColor:"gray",
    borderBottomWidth:1,
    justifyContent:"center",
    alignItems:"center",
    marginBottom:10
},titlePay:{
    marginTop:30,
    fontSize:18,
    fontWeight:"bold",
    marginBottom:15,
}, tableRowHolder:{
    flexDirection:"row",
    justifyContent:"space-between",
    marginHorizontal:20,
    marginBottom:10,
    alignItems:"center",
    textAlign:"center"
}
})