import React, { useCallback, useState } from 'react';
import { Text, View  , StyleSheet ,} from 'react-native'
import AntDesign from "react-native-vector-icons/AntDesign"
import Animated, { interpolate, useAnimatedGestureHandler, useAnimatedStyle, useSharedValue, withSpring } from 'react-native-reanimated'
import { PanGestureHandler, PanGestureHandlerGestureEvent } from 'react-native-gesture-handler'
import { runOnJS } from 'react-native-reanimated/lib/reanimated2/core'

const buttonHieght = 100 ; 

const clamp = (value: number , min:number , max:number)=>{
  'worklet';
  return Math.min(Math.max(value , min ) , max )
} 

const CartSingleItem = () => {
  const MAX_SLIDE_OFFSET = buttonHieght * .3 ;
 


  const translationyShare = useSharedValue(0)
  const translationXShare = useSharedValue(0)
  const [quantityCount , setquantityCount] = useState(0)



  const incrementQuantity = useCallback(()=>{
    setquantityCount((currentCount)=>currentCount   - 1)

  },[])

  const decrementCount = useCallback(()=>{
    setquantityCount((currentCount)=>currentCount   + 1)

  },[])

  const resetCount = useCallback(()=>{
    setquantityCount((currentCount)=> 0 )

  },[])

  const onPanEvent = useAnimatedGestureHandler<PanGestureHandlerGestureEvent>({
    onActive: (event) =>{
      translationyShare.value = clamp(event.translationY , -MAX_SLIDE_OFFSET , MAX_SLIDE_OFFSET)
      translationXShare.value = clamp(event.translationX , -MAX_SLIDE_OFFSET , 0 )
      // translationXShare.value = event.translationY
    },

    onEnd: ()=>{
      translationyShare.value = withSpring(0)
      translationXShare.value = withSpring(0)

      if(translationyShare.value === MAX_SLIDE_OFFSET){

        if (quantityCount <= 0){
          runOnJS(resetCount)();
        }else{
        runOnJS(incrementQuantity)();

        }
      }else if (translationyShare.value === -MAX_SLIDE_OFFSET) {
        // Decrement
        runOnJS(decrementCount)();
      }else if (translationXShare.value === -MAX_SLIDE_OFFSET) {
        runOnJS(resetCount)();
      }
    }
  })
  const rStyle = useAnimatedStyle(()=>{
    return {
      transform:[{translateY: translationyShare.value }  , {translateX:translationXShare.value}]
    }
  },[])

  const rPlusMinusIconStyle = useAnimatedStyle(()=>{
    const opacityY = interpolate(translationyShare.value ,  [-MAX_SLIDE_OFFSET , 0 , MAX_SLIDE_OFFSET] , [0.4 , 1 , 0.4])

    const opacityX = interpolate(translationXShare.value , 
      [-MAX_SLIDE_OFFSET , 0 ] 
      , [0 , 1])


    return {
      opacity:opacityY * opacityX
    }
  })

  const rCloseIconStyle= useAnimatedStyle(()=>{
    const opacity = interpolate(translationXShare.value , 
       [-MAX_SLIDE_OFFSET , 0 ] 
       , [0.8 , 0])
    return {
      opacity
    }
  })


  const IconSize = 20 ;
  return (

    <View style={styles.newPlusMinusHolder}>
                                
    <Animated.View style={rPlusMinusIconStyle}>
    <AntDesign name="plus" color="white"  size={IconSize}/>
    </Animated.View>
    <Animated.View style={rCloseIconStyle}>
    <AntDesign name="close" color="white" size={IconSize}  />
    </Animated.View>
    <Animated.View style={rPlusMinusIconStyle}>
    <AntDesign name="minus" color="white" size={IconSize} />
    </Animated.View>


    <View style={{...StyleSheet.absoluteFillObject , justifyContent:"center" ,alignItems:"center"}}>
    <PanGestureHandler onGestureEvent={onPanEvent}>
        <Animated.View style={[styles.counterCircle , rStyle]} >
    <Text style={styles.QuantityCount}>{quantityCount}</Text>

        </Animated.View>  
    </PanGestureHandler>

    </View> 
    </View>

  )
}

export default CartSingleItem

const styles = StyleSheet.create({
  QuantityCount:{
    color:"#fff",

  }
  ,
counterCircle:{
  width:30,
  height:30 ,
  borderRadius:100,
  backgroundColor:"#232323",
  position:"absolute", 
  justifyContent:"center",
  alignItems:"center"

}
  ,
  newPlusMinusHolder:{
      width:50,
      height:buttonHieght,
      backgroundColor:"#eb2d4a",
      borderRadius:100,
      justifyContent:"space-evenly",
      alignContent:"center",
      alignItems:"center",
      // paddingVertical:10

  },
})