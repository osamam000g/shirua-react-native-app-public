import React from 'react'
import { Text, View , StyleSheet, TextInput } from 'react-native'
import Fontisto from "react-native-vector-icons/Fontisto"

function SearchBar() {
  return (
    <View style={styles.container}>
        <TextInput style={styles.searchbar}  placeholder="Search Product..."/> 
        <Fontisto name="search" size={15} color="#eb2d4a" />
    </View>
  )
}

export default SearchBar

const styles = StyleSheet.create({
    container:{
        backgroundColor:"#fff",
        flexDirection:"row",
        paddingHorizontal:10,
        height:40,
        alignItems:"center",
        borderRadius:10,
        marginBottom:10
    },
    searchbar:{
        flex:1,
        color:"#858585",
        paddingLeft:10,
        fontSize:15
    }
})