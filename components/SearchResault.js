import React from 'react'
import { Text, View  , StyleSheet , TouchableOpacity, Image, ScrollView} from 'react-native'
import FontAwesome from "react-native-vector-icons/FontAwesome"

const items = [
  {
    id:1,
    name:require("../assets/s1.png"),
    title:"Shirt",
    price:"100"
  }
  ,
  {
    id:2,
    name:require("../assets/s1.png"),
    title:"Shirt",
    price:"100"
  }
  ,
  {
    id:3,
    name:require("../assets/s1.png"),
    title:"Shirt",
    price:"100"
  },{
    id:4,
    name:require("../assets/s1.png"),
    title:"Shirt",
    price:"100"
  },{
    id:5,
    name:require("../assets/s1.png"),
    title:"Shirt",
    price:"100"
  }


]
const SearchResault = () => {
  return (
    <View style={{marginTop:25}}>

          <View style={styles.head1}>
            <Text style={styles.headerText}>
              Reasults 
            </Text>
            <Text style={styles.seeAllText}>
              20 products
            </Text>
        </View>
        <View >
            <ScrollView showsHorizontalScrollIndicator={false}>
              <View  style={styles.container}>
                      
                      {
                      items.map((item , index)=>
                          <View  key={index} style={styles.buttonContainer}>
                          

                          <TouchableOpacity onPress={()=>openMeeting()} style={{...styles.button , backgroundColor:item.color ? item.color: "#fff"}}>
                          <Image source={item.name}  style={styles.button }/>
                          </TouchableOpacity>
                          <TouchableOpacity>
                              <Text style={styles.menuText}>{item.title}</Text>
                            <View style={styles.priceHolder}>
                                  <Text style={styles.menuText2}>90 L.E </Text>
                                  <Text style={styles.menuText1}>{item.price} L.E </Text>
                            </View>
                          </TouchableOpacity>
                          

                      </View>
                      )
                      }
                  </View>
            </ScrollView>
        
        </View>
        <TouchableOpacity style={styles.viewMoreBtnHolder}>
            <Text style={styles.viewMoreBtn}>View More</Text>
        </TouchableOpacity>
    </View>
  )
}

export default SearchResault

const styles = StyleSheet.create({
    container:{
      marginTop:25,
      flex:1
    },
    buttonContainer:{
    marginLeft:7,
    flexDirection:"row",
    marginBottom:20,
    borderColor:"#ccc",
    borderBottomWidth:1,
    paddingBottom:10
    },
    button:{
      width:100,
      height:100,
      borderRadius:5,
      padding:5,
      justifyContent:"center",
      alignItems:"center",
    },
    menuText:{
        color:'#858585',
        fontSize:20,
        paddingTop:10,
        fontWeight:"600",
        // alignSelf:"center"
        marginLeft:20
    }, 
    head1:{
      flexDirection:"row",
      justifyContent:"space-between",
      marginTop:10,
      marginHorizontal:10,
      
    },headerText:{
      fontSize:22,
  
    },
    seeAllText:{
      color:"#A09f9f",
      marginTop:7
    },priceHolder:{
        flexDirection:"row",
        // justifyContent:"space-evenly"
    },
    menuText2:{
        fontSize:12,
        textDecorationLine:"line-through",
        color:"#bbb",
        marginRight:5,
        marginLeft:20,
        marginTop:10

    },
    menuText1:{
        fontSize:16,
        marginTop:10,

        fontWeight:"bold"
    },viewMoreBtn:{
      width:150,
      height:45,
      paddingVertical:10,
      paddingHorizontal:20
      ,fontSize:18,
      color:"#000",
      backgroundColor:"#fff",
      justifyContent:"center",
      alignItems:"center",
      textAlign:"center",
      borderRadius:5,
      marginBottom:35,
      shadowColor:"#000",
      shadowOpacity:0.25,
      shadowOffset:{
        width:0,
        height:10
      },
      elevation:5
    },viewMoreBtnHolder:{
      alignItems:"center"
    }
  })