import React from 'react'
import {  View  , StyleSheet, ScrollView} from 'react-native'
import CategoryList from '../components/CategoryList'
import Footer from '../components/Footer'
import Header from '../components/Header'



const CategoriesScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
    <Header navigation={navigation}  title="All Categories" />

    <ScrollView >
            <CategoryList />
    </ScrollView>
    <Footer navigation={navigation} />
</View>
  )
}

export default CategoriesScreen

const styles = StyleSheet.create({
    container:{
        backgroundColor:"#Eeeeee",
        padding:15,
        flex:1,
    }
})