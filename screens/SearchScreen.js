import React from 'react'
import { View  , StyleSheet, ScrollView} from 'react-native'
import Footer from '../components/Footer'
import Header from '../components/Header'
import SearchBar from '../components/SearchBar'
import SearchResault from '../components/SearchResault'

const SearchScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
    <Header navigation={navigation} title="Search"/>
    <SearchBar />

    <ScrollView horizontal={false}  showsVerticalScrollIndicator={false}>
            <SearchResault />
      </ScrollView>
    <Footer  navigation={navigation}/>
</View>
  )
}

export default SearchScreen

const styles = StyleSheet.create({
  container:{
      backgroundColor:"#Eeeeee",
      padding:15,
      flex:1
  }
})