import { View, Text, StyleSheet, Image, Dimensions, TextInput } from 'react-native'
import React from 'react'
import Header from '../components/Header'
import FontAwesome from 'react-native-vector-icons/FontAwesome'
import { TouchableOpacity } from 'react-native-gesture-handler'

const width = Dimensions.get("screen").width
const height = Dimensions.get("screen").height

const RegisterScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
      <Header navigation={navigation} />
      <View style={{justifyContent:"center" ,alignItems:"center"}}>
        <Image source={require("../assets/logo.png")}  style={styles.logo}/>
      </View>
      <View style={{marginTop:30 , marginHorizontal:20}}>
      <View style={styles.inputHolder}> 
            <FontAwesome name="user" size={20} style={styles.inputIcon}/>
            <TextInput  placeholder='Your Name' />
        </View>
        <View style={styles.inputHolder}> 
            <FontAwesome name="envelope" size={20} style={styles.inputIcon}/>
            <TextInput  placeholder='Enter Your Email' />
        </View>
       
        <View style={styles.inputHolder}>
            <FontAwesome name="key" size={20} style={styles.inputIcon}/>
            <TextInput placeholder='Enter Your Password' secureTextEntry={true}/>
        </View>
        <TouchableOpacity >
            <Text style={styles.loginBtn}>
                Sign Up
            </Text>
        </TouchableOpacity>
        <View style={styles.linebreakHolder}>
            <View style={styles.linebreak}></View>
            <Text>
                OR
            </Text>
            <View style={styles.linebreak}></View>
        </View>
        <View>
            {/* <TouchableOpacity>
                <View style={styles.facebookBtn}> 
                    <FontAwesome name='facebook' size={20} style={{color:"#fff" , marginRight:10}} />
                    <Text style={styles.socialLoginText}>
                        Facebook
                    </Text>
                </View>
               
            </TouchableOpacity>

            <TouchableOpacity>
                <View style={styles.googleBtn}>
                    <FontAwesome name='google' size={20} style={{color:"#fff" ,marginRight:10}} />
                    <Text style={styles.socialLoginText}>
                        Google
                    </Text>
                </View>
            </TouchableOpacity> */}
        </View>
        <View style={{flexDirection:"row" ,justifyContent:"center" , marginTop:20}}>
            <Text style={{ fontSize:17}}>
                 have account ? 
            </Text>
            <TouchableOpacity onPress={()=>navigation.navigate("login")}>
                <Text style={{color:"#eb2d4a" , marginLeft:10 , fontSize:17}}>
                    Login
                </Text>
            </TouchableOpacity>
           
        </View>
      </View>
    </View>
  )
}

export default RegisterScreen

const styles = StyleSheet.create({
    container:{
            padding:15,
            flex:1,
            height:"100%"
    },logo:{
        width:width-100,
        height:width/4,
        marginTop:height/20,
        // resizeMode: "cover",
    },inputHolder:{
        borderBottomColor:"gray",
        borderBottomWidth:1,
        flexDirection:"row",
        height:40,
        alignItems:"center",
        padding:5,
        borderRadius:5,
        marginTop:20
        // width:width/2
    },inputIcon:{
        marginRight:10
    },loginBtn:{
        marginTop:30 ,
        // marginHorizontal:20,
        width:width-70,
        backgroundColor:"#eb2d4a",
        color:"#fff",
        height:40,
        textAlignVertical:"center",
        textAlign:"center",
        borderRadius:5,
        fontSize:18,
        textTransform:"uppercase"  
    },
    linebreakHolder:{
            marginTop:20,
            justifyContent:"space-between",
            flexDirection:"row",
            height:20,
            alignItems:"center"
    },
    linebreak:{
        width:width/2-60,
        borderBottomColor:"gray",
        borderBottomWidth:1,
        justifyContent:"center",
        alignItems:"center"
    },
    facebookBtn:{
        marginTop:20 ,
        // marginHorizontal:20,
        width:width-70,
        flexDirection:"row",
        backgroundColor:"#3b5998",
        color:"#fff",
        height:40,
        justifyContent:"center",
        alignItems:"center",
        borderRadius:5,
        fontSize:18,
        textTransform:"uppercase"  
    },
    googleBtn:{
        marginTop:15 ,
        // marginHorizontal:20,
        width:width-70,
        backgroundColor:"#ea4335",
        color:"#fff",
        height:40,
        flexDirection:"row",
        justifyContent:"center",
        alignItems:"center",
        borderRadius:5,
        fontSize:18,
        textTransform:"uppercase"  
    },
    socialLoginText:{
        color:"#fff",
        fontSize:18,
        textTransform:"uppercase"  
    }
})