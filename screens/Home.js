import React from 'react'
import { SafeAreaView, View  , StyleSheet, ScrollView} from 'react-native'
import Footer from '../components/Footer'
// import ContactsMenu from '../components/ContactsMenu'
import Header from '../components/Header'
import HomeSlider from '../components/HomeSlider'
import MenuButtons from '../components/MenuButtons'
import NewArrival from '../components/NewArrival'
import SearchBar from '../components/SearchBar'


function Home({navigation}) {
  return (
    <View style={styles.container}>
            <Header navigation={navigation} title="home"/>
            <SearchBar />

        <ScrollView horizontal={false}  showsVerticalScrollIndicator={false}>
            <HomeSlider />
                   <MenuButtons  navigation={navigation} />
                <NewArrival navigation={navigation} />
                <NewArrival navigation={navigation} />
                <NewArrival navigation={navigation} />
            {/* menu buttons  */}
            {/* contacts menu  */}
          </ScrollView>
            <Footer navigation={navigation} />
    </View>
  )
}

export default Home

const styles = StyleSheet.create({
    container:{
        backgroundColor:"#Eeeeee",
        padding:15,
        flex:1
    }
})