import { View, Text, ScrollView, StyleSheet, Dimensions } from 'react-native'
import React from 'react'
import Feather from "react-native-vector-icons/Feather"
import Ionicons from "react-native-vector-icons/Ionicons"
import Footer from '../components/Footer'
import Header from '../components/Header'

const width = Dimensions.get("screen").width
const ProfileScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
        <ScrollView style={styles.ScrollView}>

            <Header title="My Account" navigation={navigation} />
            <View style={styles.userInfo}>
            {/* User Info */}
                <View style={styles.userIconHolder}>
                     <Feather name="user" size={50} color="gray" />
                </View>
                <Text style={styles.username}>Mohamed Osama</Text>
            </View>
            <View>
                {/* Profile Body  */}
                <View style={styles.userInfoLinksHolder}>
                    <View style={styles.infoLink}>
                        <View style={styles.linkIconText}>
                            <Feather name="user" size={22} color="gray" />
                            <Text style={{fontSize:18 , paddingLeft:20 }}>Account </Text>
                        </View>
                        
                        <Feather name="arrow-right" size={20} color="gray" />

                    </View>
                    <View style={styles.infoLink}>
                        <View style={styles.linkIconText}>
                            <Feather name="shopping-cart" size={22} color="gray" />
                            <Text style={{fontSize:18 , paddingLeft:20}}>Orders </Text>
                        </View>
                        
                        <Feather name="arrow-right" size={20} color="gray" />

                    </View>
                    <View style={styles.infoLink}>
                        <View style={styles.linkIconText}>
                            <Ionicons name="exit-outline" size={22} color="gray" />
                            <Ionicons name="enter-outline" size={22} color="gray" />
                            <Text style={{fontSize:18 , paddingLeft:20}}>Logout / Login & Register </Text>
                        </View>
                        
                        {/* <Feather name="arrow-right" size={20} color="gray" /> */}

                    </View>
                </View>
            </View>
        </ScrollView>
        <Footer navigation={navigation} />
      
    </View>
  )
}

export default ProfileScreen

const styles = StyleSheet.create({
    container:{
        paddingVertical:15,
        paddingHorizontal:5,
        flex:1

    },
    ScrollView:{
        flex:1
    },
    userInfo:{
        justifyContent:"center",
        alignItems:"center",
        height:250,
        backgroundColor:"#Eeb4be",
        width:width-10,
        borderRadius:5,
        // marginTop:20,
        shadowColor:"#000",
        shadowOffset:{
            width:0,
            height:10
        },
        shadowOpacity:0.25,
        elevation:5,
        shadowRadius:5
    },userIconHolder:{
        width:80,
        height:80,
        borderRadius:50,
        backgroundColor:"#fff",
        justifyContent:"center",
        alignItems:"center",
        marginBottom:20,
    },username:{
        fontSize:20,
        color:"grey",
    },userInfoLinksHolder:{
        width:width-50,
        // paddingHorizontal:20,
        backgroundColor:"#fff",
        alignItems:"center",
        alignSelf:"center",
        marginTop:-20,
        borderRadius:5,
        shadowColor:"#000",
        shadowOffset:{
            width:0,
            height:10
        },
        shadowOpacity:0.25,
        shadowRadius:5,
        elevation:5
    },
    infoLink:{
        flexDirection:"row",
        justifyContent:"space-between",
        height:60,
        alignItems:"center",
        // backgroundColor:"red",
        width:"100%",
        paddingHorizontal:20,
        borderBottomColor:"#ccc",
        borderBottomWidth:1
    },
    linkIconText:{
        flexDirection:"row",
        // justifyContent:"flex-start",
        // width:"35%",
    
    }

})