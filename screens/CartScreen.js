import React from 'react'
import { View  , StyleSheet, ScrollView, TouchableOpacity, Text, Dimensions, TextInput} from 'react-native'
import CartItems from '../components/CartItems'
import Footer from '../components/Footer'
import Header from '../components/Header'
import SearchBar from '../components/SearchBar'


const width = Dimensions.get("screen").width

const CartScreen = ({navigation}) => {
  return (
    <View style={styles.container}>
      <ScrollView horizontal={false}  showsVerticalScrollIndicator={false}>
              <CartItems />
              <View style={{flexDirection:"row",justifyContent:"space-between" ,alignItems:"center"}}>
                  <View>
                      <TextInput style={styles.TextInput} placeholder='Coupon Code ' />
                  </View>
                  <TouchableOpacity style={styles.coupnHolder}>
                      <Text style={styles.CouponBtn}>
                          Apply
                      </Text>
                  </TouchableOpacity>
              </View>
        </ScrollView>
    </View>
  )
}

export default CartScreen

const styles = StyleSheet.create({
  container:{
      backgroundColor:"#fff",
      // padding:15,
      // flex:1,
      // height:"100%",
      width:width-30
  },viewMoreBtn:{
    width:width/2-20,
    height:45,
    paddingVertical:10,
    paddingHorizontal:20
    ,fontSize:18,
    color:"#000",
    backgroundColor:"#fff",
    justifyContent:"center",
    alignItems:"center",
    textAlign:"center",
    borderRadius:5,
    // marginBottom:35,
    shadowColor:"#000",
    shadowOpacity:0.25,
    shadowOffset:{
      width:0,
      height:10
    },
    elevation:5
  },viewMoreBtnHolder:{
    flexDirection:"row",
    justifyContent:"space-between"
    // alignItems:"flex-end"
  },CouponBtn:{
    marginTop:30 ,
    // marginHorizontal:20,
    width:width/2.5-35,
    backgroundColor:"#eb2d4a",
    color:"#fff",
    height:40,
    textAlignVertical:"center",
    textAlign:"center",
    borderRadius:5,
    fontSize:18,
    // textTransform:"uppercase"  
},coupnHolder:{
  flexDirection:"row",
  justifyContent:"space-around"
},TextInput:{
  width:width/1.5-35,
  borderWidth:1,
  borderColor:"gray",
  marginTop:30,
  height:40,
  borderRadius:5,
  padding:5
}
})