import { View, Text, StyleSheet, ScrollView, TouchableOpacity, Image, Dimensions, ScrollViewBase } from 'react-native'
import React from 'react'
import HeaderTransparent from '../components/HeaderTransparent'
import Feather from "react-native-vector-icons/Feather"
import Ionicons from "react-native-vector-icons/Ionicons"
const width = Dimensions.get("screen").width

const SingleProduct = ({navigation}) => {
  return (
    <View style={styles.container}>

      <HeaderTransparent title="pro" navigation={navigation} />

        <View style={{height:width , marginTop:-70}}>     
          <ScrollView  horizontal={true} style={styles.container} showsHorizontalScrollIndicator={false}>
            {/* <Text>HomeSlider</Text> */}
            <View>
              <Image style={styles.image} source={require("../assets/s1.png")} />
            </View>
            <View>
              <Image style={styles.image} source={require("../assets/s1.png")} />
            </View>
          </ScrollView>
      </View>

      <ScrollView style={{flex:1}}>
          <Text style={styles.proTitle}>

             Tomatos is Reds
          </Text>
          <View>
              {/* Starts */} 
              <View style={styles.starCountHolder}>
                  <View style={styles.starContainer}>
                    <Ionicons name="star" size={20} color="gold" />
                    <Ionicons name="star" size={20} color="gold" />
                    <Ionicons name="star" size={20} color="gold" />
                    <Ionicons name="star" size={20} color="gold" />

                      <Feather name="star" size={20} color="gold" />
                  </View>

                  <View>
                    <Text style={styles.countText}>(2)</Text>
                  </View>
               
              </View>
              <View style={styles.pricesContainer}>
                <Text style={styles.oldPrice}>
                  22 L.E
                </Text>
                <Text style={styles.newPrice}>
                   20 L.E
                </Text>
              </View>
          </View>
          <View style={{marginTop:20}}>
              <Text style={styles.btnTab}>
                  Describtion
              </Text>

              <Text style={styles.textTab}>
              Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
               when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
               It has survived not only five centuries, but also the leap into electronic typesetting, remaining e
 
              </Text>
          </View>
      </ScrollView>
      <View>
          <View style={styles.singleFooter}>
            <TouchableOpacity style={{...styles.singleFooterBtn , backgroundColor:"gold" }}>
                <Text style={{...styles.textFooter  , color:"#fff"}} >Add To Cart</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.singleFooterBtn}>
                 <Text style={{...styles.textFooter  }}  >Buy Now</Text>
            </TouchableOpacity>
          </View>
      </View>
    </View>
  )
}

export default SingleProduct

const styles = StyleSheet.create({
  container:{
      flex:1,
      paddingTop:10
  },image:{
        width:width,
        height:width,     
    },
    starContainer:{
      flexDirection:"row",
      justifyContent:"flex-start",

    },proTitle:{
      fontSize:22,
      fontWeight:"bold",
      marginTop:10,
      paddingLeft:10

    },starCountHolder:{
      flexDirection:"row",
      marginTop:10,
      paddingLeft:15

    },countText:{
      marginLeft:10
    },
    btnTab:{
      backgroundColor:"#fff",
      width:width*0.33,
      height:40,
      textAlignVertical:"center",
      textAlign:"center",
      borderRadius:5,
      fontWeight:"bold"
      // marginLeft:10,
    },textTab:{
      backgroundColor:"#fff",
      padding:20
    },pricesContainer:{
      flexDirection:"row",

      marginLeft:15,
      marginTop:10
    },oldPrice:{
      fontSize:18,
        textDecorationLine:"line-through",
        color:"red",
        opacity:0.4,
        marginRight:12
    },newPrice:{
      fontSize:20,
        color:"black",
        fontWeight:"bold"
        // opacity:0.4
    },singleFooter:{
      flexDirection:"row",
      justifyContent:"space-between",
      width:width-20,
      marginTop:25
      // marginHorizontal:10
    },singleFooterBtn:{
      width:"50%",
      backgroundColor:"#eb2d4a",
      marginHorizontal:5,
      justifyContent:"center",
   
      height:40,
      bottom:10,
      borderRadius:5,
      shadowColor:"#000",
      shadowOffset:{
        width:0,
        height:10
      },
      shadowOpacity:0.25,
      shadowRadius:5,
      elevation:5
    },
    textFooter:{
      textAlign:"center",
      textAlignVertical:"center",
      color:"#fff",

    }

})