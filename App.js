import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Home from './screens/Home';
import { createDrawerNavigator } from '@react-navigation/drawer';
import SearchScreen from "./screens/SearchScreen"
import { NavigationContainer } from '@react-navigation/native';
import CustomDrawer from './components/CustomDrawer';
import Feather  from "react-native-vector-icons/Feather"
import CategoriesScreen from './screens/CategoriesScreen';
import ProfileScreen from './screens/ProfileScreen';
import SingleProduct from './screens/SingleProduct';
import CartScreen from './screens/CartScreen';
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import AddressScreen from './screens/AddressScreen';

const Drawer = createDrawerNavigator();
export default function App() {

  return (
    // <View style={styles.container}>
    <NavigationContainer>
      <Drawer.Navigator drawerContent={props=><CustomDrawer {...props}/>}
        screenOptions={{  drawerActiveBackgroundColor:"#eb2d4a" , drawerActiveTintColor:"#fff",drawerLabelStyle:{marginLeft:-25}}}
      >
        <Drawer.Screen name="home" component={Home} options={{headerShown:false , drawerIcon:({color})=><Feather name='home' size={22} color={color} />}} />
        <Drawer.Screen name="search" component={SearchScreen} options={{headerShown:false , drawerIcon:({color})=><Feather name="search" size={22} color={color} /> }}  />
        <Drawer.Screen name="categories" component={CategoriesScreen} options={{headerShown:false , drawerIcon:({color})=><Feather name="grid" size={22} color={color} /> }}  />
        <Drawer.Screen name="profile" component={ProfileScreen} options={{headerShown:false , drawerIcon:({color})=><Feather name="user" size={22} color={color} /> }}  />
        <Drawer.Screen name="single" component={SingleProduct} 
         options={{
          headerShown:false 
        //   drawerItemStyle: { height: 0 }
        }}
         />
         <Drawer.Screen name="cart" component={AddressScreen} 
         options={{
          headerShown:false 
        //   drawerItemStyle: { height: 0 }
        }}
         />
          <Drawer.Screen name="login" component={LoginScreen} 
         options={{
          headerShown:false 
        //   drawerItemStyle: { height: 0 }
        }}
         />
            <Drawer.Screen name="register" component={RegisterScreen} 
         options={{
          headerShown:false 
        //   drawerItemStyle: { height: 0 }
        }}
         />
             <Drawer.Screen name="address" component={AddressScreen} 
         options={{
          headerShown:false 
        //   drawerItemStyle: { height: 0 }
        }}
         />
      </Drawer.Navigator>
      </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
